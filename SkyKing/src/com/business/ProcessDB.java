package com.business;

import java.sql.*;
import java.util.*;


public class ProcessDB {
	
	Connection con=null;
	
	public Connection dbConnection()
	{		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			 con=DriverManager.getConnection("jdbc:mysql://localhost:3306/flightapp?useSSL=false","root","admin");
			 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
	
	public  boolean doSelect(String email,String pass)
	{
		boolean st = false;
		try {
			Connection con=dbConnection();
			String sql = "select * from useraccess where email=? and pass=?";            
            PreparedStatement preparedStmt = con.prepareStatement(sql);
            preparedStmt.setString(1, email);
            preparedStmt.setString(2, pass);
            ResultSet rs =preparedStmt .executeQuery();
            
            String em = null;
            String pa = null;
            
            while(rs.next())
            {
            	em = rs.getString(3);
            	pa = rs.getString(4);
            }
            
            if(em.equals(email) && pa.equals(pass))
            {
            	st = true;
            }
            
            else
            {
            	st = false;
            }
            
            con.close();			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return st;		
	}
	
	public  ArrayList<UserPojo> SelectName(String sour,String dest)
	{
		ArrayList<UserPojo> dataLst=new ArrayList<UserPojo>();
		try {
			Connection con=dbConnection();
			String sql = "select * from flight where usource=? and udest=?";            
            PreparedStatement preparedStmt = con.prepareStatement(sql);
            preparedStmt.setString(1, sour);
            preparedStmt.setString(2, dest);
            ResultSet rs =preparedStmt .executeQuery();
            
            while(rs.next())
    		{   
        		UserPojo up = new UserPojo();
    			String from = rs.getString(2);
    			String to = rs.getString(3);
    			String fname = rs.getString(4);
    			String as = rs.getString(5);
    			
    			up.setSour(from);
    			up.setDest(to);    			
    			up.setFname(fname);
    			up.setAseats(as);
    			
    			dataLst.add(up);    			
    		}               
            
            con.close();			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataLst;		
	}	
	
	public void doInsert(String name,String email,String pass)
	{
		try
		{
			con = dbConnection();
            String sql = "INSERT INTO useraccess (name, email, pass)" + "VALUES (?, ?, ?)";            
            PreparedStatement preparedStmt = con.prepareStatement(sql);
            preparedStmt.setString(1, name);
            preparedStmt.setString(2, email);
            preparedStmt.setString(3, pass);
            preparedStmt.executeUpdate();
            con.close();			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}	
	}
	
	public void doUpdate(String as)
	{
		try
		{
			System.out.println("seats >>>"+as);
			con = dbConnection();
			Statement st = con.createStatement();
			String sql = "UPDATE flight SET avalaibleseats = avalaibleseats - 1 where avalaibleseats = ? "; 
			PreparedStatement preparedStmt = con.prepareStatement(sql);
            preparedStmt.setString(1, as);
            preparedStmt.executeUpdate();            
			con.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}	
	}
}
