package com.business;

public class UserPojo 
{
	private String sour;
	private String dest;
	private String fname;
	private String aseats;
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getAseats() {
		return aseats;
	}
	public void setAseats(String aseats) {
		this.aseats = aseats;
	}
	public String getSour() {
		return sour;
	}
	public void setSour(String sour) {
		this.sour = sour;
	}
	public String getDest() {
		return dest;
	}
	public void setDest(String dest) {
		this.dest = dest;
	}
	
}
