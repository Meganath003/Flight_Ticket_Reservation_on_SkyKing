package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.business.ProcessDB;

/**
 * Servlet implementation class DetailsServ
 */
@WebServlet("/DetailsServ")
public class DetailsServ extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailsServ() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		ProcessDB p = new ProcessDB();
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String pass = request.getParameter("pass");
		String cpass = request.getParameter("cpass");		
			
		if(pass.equals(cpass))
		{
			 out.println("<script type=\"text/javascript\">");
			 out.println("alert('You Were Successfully Register on SkyKing');");
			 out.println("location='index.html';");
			 out.println("</script>");		 
			 p.doInsert(name, email, cpass);
		}
		else
		{
			   out.println("<script type=\"text/javascript\">");
		       out.println("alert('Password and Confirm Password must be Same');");
		       out.println("location='signup.html';");
		       out.println("</script>");		           
		 }
		}
	}

